module.exports = function(grunt) {

	grunt.initConfig({
    	
		pkg: grunt.file.readJSON('package.json'),

		clean: {
			dist: [
		        'dist/',
		        '.build/',
		        'reports/'
	        ]
		},

		connect: {
		    server: {
		        options: {
		            port: 8000,
		            base: 'dist/'
		        }
		    }
		},

		copy: {
			css: {
				expand: true,
				cwd: 'app/css/',
				src: '**/*.css',
				dest: 'dist/css/',
			},
			html: {
				expand: true,
				cwd: 'app/html/',
			    src: '**/*.html',
			    dest: 'dist/',
			}
		},

		intern: {
			client: {
				options: {
					config: '.build/tests/intern'
				}
			}
		},

		ts: {
			options: {
				failOnTypeErrors: true,
				module: 'amd',
				target: 'es5',
				noImplicitAny: true,
				fast: 'never',
				preserveConstEnums: true
			},
			app: {
				src: [ 'app/ts/**/*.ts' ],
				outDir: 'dist/js/',
				options: {
					sourceMap: false
				}
			},
			tests: {
				src: [ 'app/ts/**/*.ts', 'node_modules/intern/typings/intern/intern.d.ts', 'tests/**/*.ts' ],
				outDir: '.build/',
				options: {
					moduleResolution: 'classic',
					sourceMap: true
				}
			}
		},

	    tslint: {
	        options: {
	            configuration: "tslint.json"
	        },
	        files: {
	            src: [
	                "app/ts/**/*.ts"
	            ]
	        }
	    },

	    watch: {
	    	options: {
	    		livereload: true,
	    	},
	    	html: {
	    		files: 'app/**/*.html',
	    		tasks: ['copy'],
	    	},
	    	ts: {
	    		files: 'app/ts/**/*.ts',
	    		tasks: ['ts', 'intern:client'],
	    	},
	    	tests: {
	    		files: 'tests/**/*.spec.ts',
	    		tasks: ['ts', 'intern:client'],
	    	}
	    }
	});

	grunt.loadNpmTasks('intern');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-ts');
	grunt.loadNpmTasks('grunt-tslint');
	grunt.loadNpmTasks('grunt-typescript');

	grunt.registerTask('default', [
        'clean',
	    'tslint',
	    'ts:tests',
	    'intern:client',
	    'ts:app',
       	'copy',
       	'connect',
       	'watch'
   	]);

	grunt.registerTask('test', [ 'ts:tests', 'intern:client' ]);

};
