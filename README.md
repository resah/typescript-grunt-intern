# TypeScript for Web Sandbox

Sandbox to play with

* TypeScript
* Grunt
* Intern
* Testing the whole lot


## Sources

* [Testing Typescript with Intern](https://www.sitepen.com/blog/2015/03/24/testing-typescript-with-intern/)
* [Modules in TypeScript](http://typescript.codeplex.com/wikipage?title=Modules%20in%20TypeScript&referringTitle=TypeScript%20Documentation)
* [Mayhem](https://github.com/SitePen/mayhem)