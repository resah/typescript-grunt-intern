import personModule = require('./Person');
import studentModule = require('./Student');

  export function greeter(person: personModule.Person): string {
      return 'Hello, ' + person.firstName;
  }

  let user: studentModule.Student;
  user = new studentModule.Student('John', 'E', 'Watson');

  document.body.innerHTML = greeter(user);
