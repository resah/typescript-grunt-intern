export interface Person {
    firstName: string;
    lastName: string;
    doSomething(source: string): boolean;
}
