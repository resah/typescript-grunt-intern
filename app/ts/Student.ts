import personModule = require('./Person');

export class Student implements personModule.Person {
    protected fullName: string;
    constructor(public firstName: string, public middleInitial: string, public lastName: string) {
        this.fullName = firstName + ' ' + middleInitial + ' ' + lastName;
    }

    doSomething(source: string): boolean {
      return false;
    }
}
