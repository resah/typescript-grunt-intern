// Configuration options for the module loader; any AMD configuration options supported by the specified AMD loader
// can be used here
export var loaderOptions = {
  // Packages that should be registered with the loader in each testing environment
  packages: [
    //{ name: 'dojo-core', location: 'node_modules/dojo-core' },
    { name: 'app', location: '.build/app' },
    { name: 'tests', location: '.build/tests' }
  ]
};

export var reporters = [
    {
        'id': 'Console',
        'watermarks': {
            statements: [ 70, 80 ],
            branches: [ 80, 90 ],
            functions: [ 70, 80 ],
            lines: [ 80, 90 ]
        }
    },
    {
        'id': 'LcovHtml',
        'directory': 'reports/',
        'watermarks': {
            statements: [ 70, 80 ],
            branches: [ 80, 90 ],
            functions: [ 70, 80 ],
            lines: [ 80, 90 ]
        }
    }
];

// Non-functional test suite(s) to run in each browser
export var suites = [ 'tests/unit/main.spec' ];

// Functional test suite(s) to run in each browser once non-functional tests are completed
//export var functionalSuites = [ 'tests/functional/all' ];

// A regular expression matching URLs to files that should not be included in code coverage analysis
export var excludeInstrumentation = /(?:node_modules|bower_components|tests)[\/\\]/;
