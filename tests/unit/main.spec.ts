import registerSuite = require('intern!object');
import assert = require('intern/chai!assert');
import studentModule = require('app/ts/Student');

registerSuite({
    name: 'Test student',
    'default data'() {
        var s = new studentModule.Student("John", "E", "Watson");
      
        assert.strictEqual(s.firstName, 'John',
                    'First name should be "John"');
      
        assert.strictEqual(s.doSomething('abc'), false,
                    'Should not do something');
    }
});
